neochat (22.11-3) UNRELEASED; urgency=medium

  [ Aurélien COUDERC ]
  * Change maintainer to the main Debian Qt/KDE Maintainers team instead
    of Extras.

  [ Rik Mills ]
  * Add checkatomics-to-fix-riscv64-build.patch to fix FTBFS on riscv64.
    Thank you to Gianfranco Costamagna.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Mon, 26 Dec 2022 21:40:52 +0100

neochat (22.11-2) unstable; urgency=medium

  [ Sandro Knauß ]
  * Add all missing qml depdendencies.

 -- Sandro Knauß <hefee@debian.org>  Thu, 08 Dec 2022 14:02:11 +0100

neochat (22.11-1) unstable; urgency=medium

  [ Sandro Knauß ]
  * New upstream release.
  * Update build-deps and deps with the info from cmake.
  * Update copyright for 22.11.

 -- Sandro Knauß <hefee@debian.org>  Tue, 06 Dec 2022 22:13:11 +0100

neochat (22.09-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump KF packages to 5.91.0
    - drop libkf5sonnet-dev, no more needed
    - explicitly add gettext

 -- Pino Toscano <pino@debian.org>  Sat, 22 Oct 2022 12:39:06 +0200

neochat (22.06-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove the unused debian/meta/ stuff.
  * Bump Standards-Version to 4.6.1, no changes required.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - drop all the manually specified addons and buildsystem for dh
  * Minor updates to copyright.

 -- Pino Toscano <pino@debian.org>  Fri, 24 Jun 2022 21:10:55 +0200

neochat (22.04-1) unstable; urgency=medium

  * New upstream release (22.04).
  * Update copyright information for 22.04.

 -- Sandro Knauß <hefee@debian.org>  Mon, 16 May 2022 21:21:45 +0200

neochat (22.02-2) unstable; urgency=medium

  [ Sandro Knauß ]
  * Fix copyright issue, as some files are released under different
    licenses.

 -- Sandro Knauß <hefee@debian.org>  Mon, 02 May 2022 15:01:37 +0200

neochat (22.02-1) unstable; urgency=medium

  [ Norbert Preining ]
  * Prepare package for Debian.

  [ Sandro Knauß ]
  * First upload to Debian (Closes: #989529)
  * Update Vcs links to salsa.
  * Use new dev package name of qtkechain (qtkeychain-qt5-dev).
  * Add qcoro to BDs.
  * Enable hardening.
  * Make signing key minimal.
  * Add upstream/metadata.
  * Write a better short and long description.
  * Update copyright for 22.02.
  * Update maintainer.
  * Add myself to Uploades.

 -- Sandro Knauß <hefee@debian.org>  Sun, 27 Feb 2022 04:33:59 +0100
